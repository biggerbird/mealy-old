# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

build_number = System.cmd("git", ["rev-list", "HEAD", "--count"]) |> elem(0) |> String.trim()

config :mealy,
  ecto_repos: [Mealy.Repo],
  generators: [binary_id: true],
  version: Mealy.MixProject.project()[:version],
  build_number: build_number,
  release_built_at: DateTime.utc_now()

# Configures the endpoint
config :mealy, MealyWeb.Endpoint,
  url: [host: "mealy.lvh.me"],
  secret_key_base: "+lE8JpWATQz+BBFDABofPj6Th1Ouq2KTUzMG8gzdIjChsK+S/pTL4D/whp+9kZbW",
  render_errors: [view: MealyWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Mealy.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "Rk3WGXOmOLwMSX9uTaN6aUh1RrRdehHb"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :sentry,
  dsn: "https://d57ccf1d14bb49e6a969354bd4b86635@sentry.io/1838238",
  environment_name: Mix.env(),
  enable_source_code_context: true,
  root_source_code_path: File.cwd!(),
  included_environments: [:prod]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
