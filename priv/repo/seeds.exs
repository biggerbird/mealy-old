# Script for populating the database. To run:
#   mix run priv/repo/seeds.exs
#
# To rebuild database schema:
#   mix ecto.drop && mix ecto.create && mix ecto.migrate

alias Mealy.Lists.MixedList
alias Mealy.People.Person
alias Mealy.Repo
alias Mealy.Teams.{Team, TeamMembership}

attrs = %{name: "A&A"}
team = Repo.get_by(Team, attrs) || Repo.insert!(Map.merge(%Team{}, attrs))

attrs = %{name: "Aaron"}
aaron = Repo.get_by(Person, attrs) || Repo.insert!(Map.merge(%Person{}, attrs))

attrs = %{team_id: team.id, person_id: aaron.id}
Repo.get_by(TeamMembership, attrs) || Repo.insert!(Map.merge(%TeamMembership{}, attrs))

attrs = %{name: "Annie"}
annie = Repo.get_by(Person, attrs) || Repo.insert!(Map.merge(%Person{}, attrs))

attrs = %{team_id: team.id, person_id: annie.id}
Repo.get_by(TeamMembership, attrs) || Repo.insert!(Map.merge(%TeamMembership{}, attrs))

order = 0

attrs = %{name: "Meal Planning", primary: true, order: order}
primary_list = Repo.get_by(MixedList, attrs) || Repo.insert!(Map.merge(%MixedList{}, attrs))

order = order + 1
attrs = %{name: "Want to eat", default: true, parent_id: primary_list.id, order: order}
Repo.get_by(MixedList, attrs) || Repo.insert!(Map.merge(%MixedList{}, attrs))

order = order + 1
attrs = %{date: ~D[2020-04-09], parent_id: primary_list.id, order: order}
Repo.get_by(MixedList, attrs) || Repo.insert!(Map.merge(%MixedList{}, attrs))

order = order + 1
attrs = %{date: ~D[2020-04-10], parent_id: primary_list.id, order: order}
Repo.get_by(MixedList, attrs) || Repo.insert!(Map.merge(%MixedList{}, attrs))

order = order + 1
attrs = %{date: ~D[2020-04-11], parent_id: primary_list.id, order: order}
Repo.get_by(MixedList, attrs) || Repo.insert!(Map.merge(%MixedList{}, attrs))

order = order + 1
attrs = %{date: ~D[2020-04-12], parent_id: primary_list.id, order: order}
Repo.get_by(MixedList, attrs) || Repo.insert!(Map.merge(%MixedList{}, attrs))
