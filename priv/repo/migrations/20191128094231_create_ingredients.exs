defmodule Mealy.Repo.Migrations.CreateIngredients do
  use Ecto.Migration

  def change do
    create table(:ingredients, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :team_id, :uuid
      add :canonical_id, :string

      add :name, :string
      add :type, :string
      add :subtype, :string
      add :plural, :string
      add :unit, :string
      add :unit_type, :string

      add :external_id, :string
      add :typical_shelf_life_days, :integer
      add :target_stock_level, :integer

      timestamps()
    end

    create table(:ingredient_tags, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :ingredient_id, :uuid
      add :tag_id, :uuid

      timestamps()
    end
  end
end
