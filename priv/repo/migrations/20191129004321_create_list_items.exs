defmodule Mealy.Repo.Migrations.CreateListItems do
  use Ecto.Migration

  def change do
    create table(:list_items, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :copied_from_id, :uuid

      add :list_id, :uuid
      add :order, :integer

      add :label, :string
      add :note, :text

      # dish/recipe fields
      add :dish_id, :uuid
      add :recipe_scale, :float
      add :scheduled_at, :utc_datetime

      # ingredient fields
      add :ingredient_id, :uuid
      add :bought_at, :utc_datetime
      add :expires_at, :utc_datetime

      # status fields
      add :completed, :boolean, default: false, null: false
      add :completed_at, :utc_datetime
      # e.g. made, used, expired
      add :completed_verb, :string

      timestamps()
    end

    create table(:list_item_tags, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :list_item_id, :uuid
      add :tag_id, :uuid

      timestamps()
    end
  end
end
