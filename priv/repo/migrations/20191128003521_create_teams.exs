defmodule Mealy.Repo.Migrations.CreateTeams do
  use Ecto.Migration

  def change do
    create table(:teams, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :parent_id, :uuid

      add :name, :string

      timestamps()
    end
  end
end
