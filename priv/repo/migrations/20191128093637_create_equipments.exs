defmodule Mealy.Repo.Migrations.CreateEquipments do
  use Ecto.Migration

  def change do
    create table(:equipments, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :team_id, :uuid
      add :canonical_id, :uuid

      add :name, :string
      add :type, :string
      add :subtype, :string

      timestamps()
    end

    create table(:equipment_tags, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :equipment_id, :uuid
      add :tag_id, :uuid

      timestamps()
    end
  end
end
