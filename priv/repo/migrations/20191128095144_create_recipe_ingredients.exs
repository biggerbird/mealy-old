defmodule Mealy.Repo.Migrations.CreateRecipeIngredients do
  use Ecto.Migration

  def change do
    create table(:recipe_ingredients, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :section_id, :uuid
      add :ingredient_id, :uuid

      add :quantity, :string
      add :prep, :string
      add :note, :string

      timestamps()
    end
  end
end
