defmodule Mealy.Repo.Migrations.CreateTeamMemberships do
  use Ecto.Migration

  def change do
    create table(:team_memberships, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :team_id, :uuid
      add :person_id, :uuid

      add :role, :string

      timestamps()
    end

  end
end
