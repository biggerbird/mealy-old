defmodule Mealy.Repo.Migrations.CreateDishes do
  use Ecto.Migration

  def change do
    create table(:dishes, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :owner_id, :uuid
      add :team_id, :uuid
      add :canonical_id, :uuid
      add :copied_from_id, :uuid

      add :name, :string
      add :description, :text

      # main, side, drink, etc.
      add :type, :integer

      # whether it is something to be made (vs. bought/ordered)
      add :recipe, :boolean, default: false

      add :yield_min, :integer
      add :yield_max, :integer
      add :yield_unit, :string

      add :public, :boolean, default: false

      timestamps()
    end

    create table(:dish_tags, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :dish_id, :uuid
      add :tag_id, :uuid

      timestamps()
    end
  end
end
