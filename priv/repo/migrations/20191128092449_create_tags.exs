defmodule Mealy.Repo.Migrations.CreateTags do
  use Ecto.Migration

  def change do
    create table(:tags, primary_key: false) do
      add :id, :binary_id, primary_key: true

      # optional, we could have system-wide tags
      add :team_id, :uuid

      add :name, :string

      timestamps()
    end
  end
end
