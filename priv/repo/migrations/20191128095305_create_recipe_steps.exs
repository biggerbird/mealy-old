defmodule Mealy.Repo.Migrations.CreateRecipeSteps do
  use Ecto.Migration

  def change do
    create table(:recipe_steps, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :section_id, :uuid

      add :content, :text

      add :days_ahead, :integer
      add :minutes_ahead, :integer

      timestamps()
    end
  end
end
