defmodule Mealy.Repo.Migrations.CreateLists do
  use Ecto.Migration

  def change do
    create table(:lists, primary_key: false) do
      add :id, :binary_id, primary_key: true

      add :team_id, :uuid
      add :person_id, :uuid
      add :parent_id, :uuid

      add :name, :string
      add :note, :text

      add :date, :date
      add :time, :utc_datetime

      add :order, :integer

      add :primary, :boolean, default: false, null: false
      add :default, :boolean, default: false, null: false

      timestamps()
    end
  end
end
