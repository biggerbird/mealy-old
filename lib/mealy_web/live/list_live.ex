defmodule MealyWeb.ListLive do
  use Phoenix.LiveView, layout: {MealyWeb.LayoutView, "live.html"}

  alias Mealy.Lists
  alias Mealy.Lists.ListTodo

  def render(assigns) do
    Phoenix.View.render(MealyWeb.ListView, "show.html", assigns)
  end

  def mount(%{"id" => id}, _session, socket) do
    list = Lists.get_list!(id)
    # FIXME
    tz = Timex.timezone("Asia/Seoul", Timex.now())
    today = Timex.now(tz) |> Timex.to_date()

    days =
      0..6
      |> Enum.map(&Date.add(today, &1))
      |> Enum.map(&{&1, Timex.format!(&1, "%A %-m/%-d", :strftime)})

    todos = [%ListTodo{name: "Testing"}]

    socket =
      assign(socket,
        list: list,
        pinned_lists: Lists.list_lists(list.id),
        days: days,
        todos: todos
      )

    {:ok, socket}
  end
end
