defmodule MealyWeb.TodoComponent do
  use Phoenix.LiveComponent

  def render(assigns) do
    Phoenix.View.render(MealyWeb.ListView, "todo.html", assigns)
  end
end
