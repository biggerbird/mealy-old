defmodule MealyWeb.DayComponent do
  use Phoenix.LiveComponent

  def render(assigns) do
    Phoenix.View.render(MealyWeb.ListView, "day.html", assigns)
  end
end
