defmodule MealyWeb.ListComponent do
  use Phoenix.LiveComponent

  def render(assigns) do
    Phoenix.View.render(MealyWeb.ListView, "list.html", assigns)
  end
end
