defmodule MealyWeb.Router do
  use MealyWeb, :router
  use Plug.ErrorHandler
  use Sentry.Plug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :fetch_live_flash
    plug :put_root_layout, {MealyWeb.LayoutView, "root.html"}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  # pipeline :api do
  #   plug :accepts, ["json"]
  # end

  scope "/", MealyWeb do
    pipe_through :browser

    get "/", PageController, :index

    resources "/dishes", DishController
    resources "/equipment", EquipmentController
    resources "/ingredients", IngredientController

    live "/lists/:id", ListLive, as: :list
    resources "/lists", ListController
    resources "/list_items", ListItemController
    resources "/people", PersonController
    resources "/recipe_ingredients", RecipeIngredientController
    resources "/recipe_sections", RecipeSectionController
    resources "/recipe_steps", RecipeStepController
    resources "/teams", TeamController
  end

  # Other scopes may use custom stacks.
  # scope "/api", MealyWeb do
  #   pipe_through :api
  # end
end
