defmodule MealyWeb.PageController do
  use MealyWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
