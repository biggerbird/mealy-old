defmodule MealyWeb.RecipeSectionController do
  use MealyWeb, :controller

  alias Mealy.Recipes
  alias Mealy.Recipes.RecipeSection

  def index(conn, _params) do
    recipe_sections = Recipes.list_recipe_sections()
    render(conn, "index.html", recipe_sections: recipe_sections)
  end

  def new(conn, _params) do
    changeset = Recipes.change_recipe_section(%RecipeSection{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"recipe_section" => recipe_section_params}) do
    case Recipes.create_recipe_section(recipe_section_params) do
      {:ok, recipe_section} ->
        conn
        |> put_flash(:info, "Recipe section created successfully.")
        |> redirect(to: Routes.recipe_section_path(conn, :show, recipe_section))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    recipe_section = Recipes.get_recipe_section!(id)
    render(conn, "show.html", recipe_section: recipe_section)
  end

  def edit(conn, %{"id" => id}) do
    recipe_section = Recipes.get_recipe_section!(id)
    changeset = Recipes.change_recipe_section(recipe_section)
    render(conn, "edit.html", recipe_section: recipe_section, changeset: changeset)
  end

  def update(conn, %{"id" => id, "recipe_section" => recipe_section_params}) do
    recipe_section = Recipes.get_recipe_section!(id)

    case Recipes.update_recipe_section(recipe_section, recipe_section_params) do
      {:ok, recipe_section} ->
        conn
        |> put_flash(:info, "Recipe section updated successfully.")
        |> redirect(to: Routes.recipe_section_path(conn, :show, recipe_section))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", recipe_section: recipe_section, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    recipe_section = Recipes.get_recipe_section!(id)
    {:ok, _recipe_section} = Recipes.delete_recipe_section(recipe_section)

    conn
    |> put_flash(:info, "Recipe section deleted successfully.")
    |> redirect(to: Routes.recipe_section_path(conn, :index))
  end
end
