defmodule MealyWeb.RecipeStepController do
  use MealyWeb, :controller

  alias Mealy.Recipes
  alias Mealy.Recipes.RecipeStep

  def index(conn, _params) do
    recipe_steps = Recipes.list_recipe_steps()
    render(conn, "index.html", recipe_steps: recipe_steps)
  end

  def new(conn, _params) do
    changeset = Recipes.change_recipe_step(%RecipeStep{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"recipe_step" => recipe_step_params}) do
    case Recipes.create_recipe_step(recipe_step_params) do
      {:ok, recipe_step} ->
        conn
        |> put_flash(:info, "Recipe step created successfully.")
        |> redirect(to: Routes.recipe_step_path(conn, :show, recipe_step))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    recipe_step = Recipes.get_recipe_step!(id)
    render(conn, "show.html", recipe_step: recipe_step)
  end

  def edit(conn, %{"id" => id}) do
    recipe_step = Recipes.get_recipe_step!(id)
    changeset = Recipes.change_recipe_step(recipe_step)
    render(conn, "edit.html", recipe_step: recipe_step, changeset: changeset)
  end

  def update(conn, %{"id" => id, "recipe_step" => recipe_step_params}) do
    recipe_step = Recipes.get_recipe_step!(id)

    case Recipes.update_recipe_step(recipe_step, recipe_step_params) do
      {:ok, recipe_step} ->
        conn
        |> put_flash(:info, "Recipe step updated successfully.")
        |> redirect(to: Routes.recipe_step_path(conn, :show, recipe_step))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", recipe_step: recipe_step, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    recipe_step = Recipes.get_recipe_step!(id)
    {:ok, _recipe_step} = Recipes.delete_recipe_step(recipe_step)

    conn
    |> put_flash(:info, "Recipe step deleted successfully.")
    |> redirect(to: Routes.recipe_step_path(conn, :index))
  end
end
