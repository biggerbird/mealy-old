defmodule MealyWeb.EquipmentController do
  use MealyWeb, :controller

  alias Mealy.Dishes
  alias Mealy.Dishes.Equipment

  def index(conn, _params) do
    equipments = Dishes.list_equipments()
    render(conn, "index.html", equipments: equipments)
  end

  def new(conn, _params) do
    changeset = Dishes.change_equipment(%Equipment{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"equipment" => equipment_params}) do
    case Dishes.create_equipment(equipment_params) do
      {:ok, equipment} ->
        conn
        |> put_flash(:info, "Equipment created successfully.")
        |> redirect(to: Routes.equipment_path(conn, :show, equipment))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    equipment = Dishes.get_equipment!(id)
    render(conn, "show.html", equipment: equipment)
  end

  def edit(conn, %{"id" => id}) do
    equipment = Dishes.get_equipment!(id)
    changeset = Dishes.change_equipment(equipment)
    render(conn, "edit.html", equipment: equipment, changeset: changeset)
  end

  def update(conn, %{"id" => id, "equipment" => equipment_params}) do
    equipment = Dishes.get_equipment!(id)

    case Dishes.update_equipment(equipment, equipment_params) do
      {:ok, equipment} ->
        conn
        |> put_flash(:info, "Equipment updated successfully.")
        |> redirect(to: Routes.equipment_path(conn, :show, equipment))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", equipment: equipment, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    equipment = Dishes.get_equipment!(id)
    {:ok, _equipment} = Dishes.delete_equipment(equipment)

    conn
    |> put_flash(:info, "Equipment deleted successfully.")
    |> redirect(to: Routes.equipment_path(conn, :index))
  end
end
