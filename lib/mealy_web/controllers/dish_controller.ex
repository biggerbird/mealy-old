defmodule MealyWeb.DishController do
  use MealyWeb, :controller

  alias Mealy.Dishes
  alias Mealy.Dishes.Dish

  def index(conn, _params) do
    dishes = Dishes.list_dishes()
    render(conn, "index.html", dishes: dishes)
  end

  def new(conn, _params) do
    changeset = Dishes.change_dish(%Dish{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"dish" => dish_params}) do
    case Dishes.create_dish(dish_params) do
      {:ok, dish} ->
        conn
        |> put_flash(:info, "Dish created successfully.")
        |> redirect(to: Routes.dish_path(conn, :show, dish))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    dish = Dishes.get_dish!(id)
    render(conn, "show.html", dish: dish)
  end

  def edit(conn, %{"id" => id}) do
    dish = Dishes.get_dish!(id)
    changeset = Dishes.change_dish(dish)
    render(conn, "edit.html", dish: dish, changeset: changeset)
  end

  def update(conn, %{"id" => id, "dish" => dish_params}) do
    dish = Dishes.get_dish!(id)

    case Dishes.update_dish(dish, dish_params) do
      {:ok, dish} ->
        conn
        |> put_flash(:info, "Dish updated successfully.")
        |> redirect(to: Routes.dish_path(conn, :show, dish))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", dish: dish, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    dish = Dishes.get_dish!(id)
    {:ok, _dish} = Dishes.delete_dish(dish)

    conn
    |> put_flash(:info, "Dish deleted successfully.")
    |> redirect(to: Routes.dish_path(conn, :index))
  end
end
