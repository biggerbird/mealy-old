defmodule MealyWeb.RecipeIngredientController do
  use MealyWeb, :controller

  alias Mealy.Recipes
  alias Mealy.Recipes.RecipeIngredient

  def index(conn, _params) do
    recipe_ingredients = Recipes.list_recipe_ingredients()
    render(conn, "index.html", recipe_ingredients: recipe_ingredients)
  end

  def new(conn, _params) do
    changeset = Recipes.change_recipe_ingredient(%RecipeIngredient{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"recipe_ingredient" => recipe_ingredient_params}) do
    case Recipes.create_recipe_ingredient(recipe_ingredient_params) do
      {:ok, recipe_ingredient} ->
        conn
        |> put_flash(:info, "Recipe ingredient created successfully.")
        |> redirect(to: Routes.recipe_ingredient_path(conn, :show, recipe_ingredient))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    recipe_ingredient = Recipes.get_recipe_ingredient!(id)
    render(conn, "show.html", recipe_ingredient: recipe_ingredient)
  end

  def edit(conn, %{"id" => id}) do
    recipe_ingredient = Recipes.get_recipe_ingredient!(id)
    changeset = Recipes.change_recipe_ingredient(recipe_ingredient)
    render(conn, "edit.html", recipe_ingredient: recipe_ingredient, changeset: changeset)
  end

  def update(conn, %{"id" => id, "recipe_ingredient" => recipe_ingredient_params}) do
    recipe_ingredient = Recipes.get_recipe_ingredient!(id)

    case Recipes.update_recipe_ingredient(recipe_ingredient, recipe_ingredient_params) do
      {:ok, recipe_ingredient} ->
        conn
        |> put_flash(:info, "Recipe ingredient updated successfully.")
        |> redirect(to: Routes.recipe_ingredient_path(conn, :show, recipe_ingredient))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", recipe_ingredient: recipe_ingredient, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    recipe_ingredient = Recipes.get_recipe_ingredient!(id)
    {:ok, _recipe_ingredient} = Recipes.delete_recipe_ingredient(recipe_ingredient)

    conn
    |> put_flash(:info, "Recipe ingredient deleted successfully.")
    |> redirect(to: Routes.recipe_ingredient_path(conn, :index))
  end
end
