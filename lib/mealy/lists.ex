defmodule Mealy.Lists do
  @moduledoc """
  The Lists context.
  """

  import Ecto.Query, warn: false

  alias Mealy.Lists.{ListItem, MixedList}
  alias Mealy.Repo

  @doc """
  Returns the list of lists.

  ## Examples

      iex> list_lists()
      [%MixedList{}, ...]

  """
  def list_lists(parent_id \\ nil, date \\ nil)

  def list_lists(nil, _) do
    Repo.all(from l in MixedList, where: is_nil(l.parent_id), order_by: [l.order, l.date])
  end

  def list_lists(parent_id, date) do
    query = from(l in MixedList, where: l.parent_id == ^parent_id)

    query =
      if date do
        from(l in query, where: l.date == ^date)
      else
        from(l in query, where: is_nil(l.date))
      end

    Repo.all(from l in query, order_by: [l.order, l.date])
  end

  @doc """
  Gets a single list.

  Raises `Ecto.NoResultsError` if the MixedList does not exist.

  ## Examples

      iex> get_list!(123)
      %MixedList{}

      iex> get_list!(456)
      ** (Ecto.NoResultsError)

  """
  def get_list!(id) do
    Repo.get!(MixedList, id)
    |> Repo.preload([:dishes, :ingredients])

    # |> Repo.preload(children: from(l in MixedList, order_by: l.order))
  end

  @doc """
  Creates a list.

  ## Examples

      iex> create_list(%{field: value})
      {:ok, %MixedList{}}

      iex> create_list(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_list(attrs \\ %{}) do
    %MixedList{}
    |> MixedList.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a list.

  ## Examples

      iex> update_list(list, %{field: new_value})
      {:ok, %MixedList{}}

      iex> update_list(list, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_list(%MixedList{} = list, attrs) do
    list
    |> MixedList.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a MixedList.

  ## Examples

      iex> delete_list(list)
      {:ok, %MixedList{}}

      iex> delete_list(list)
      {:error, %Ecto.Changeset{}}

  """
  def delete_list(%MixedList{} = list) do
    Repo.delete(list)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking list changes.

  ## Examples

      iex> change_list(list)
      %Ecto.Changeset{source: %MixedList{}}

  """
  def change_list(%MixedList{} = list) do
    MixedList.changeset(list, %{})
  end

  @doc """
  Returns the list of list_items.

  ## Examples

      iex> list_list_items()
      [%ListItem{}, ...]

  """
  def list_list_items do
    Repo.all(ListItem)
  end

  @doc """
  Gets a single list_item.

  Raises `Ecto.NoResultsError` if the MixedList dish does not exist.

  ## Examples

      iex> get_list_item!(123)
      %ListItem{}

      iex> get_list_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_list_item!(id), do: Repo.get!(ListItem, id)

  @doc """
  Creates a list_item.

  ## Examples

      iex> create_list_item(%{field: value})
      {:ok, %ListItem{}}

      iex> create_list_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_list_item(attrs \\ %{}) do
    %ListItem{}
    |> ListItem.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a list_item.

  ## Examples

      iex> update_list_item(list_item, %{field: new_value})
      {:ok, %ListItem{}}

      iex> update_list_item(list_item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_list_item(%ListItem{} = list_item, attrs) do
    list_item
    |> ListItem.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ListItem.

  ## Examples

      iex> delete_list_item(list_item)
      {:ok, %ListItem{}}

      iex> delete_list_item(list_item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_list_item(%ListItem{} = list_item) do
    Repo.delete(list_item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking list_item changes.

  ## Examples

      iex> change_list_item(list_item)
      %Ecto.Changeset{source: %ListItem{}}

  """
  def change_list_item(%ListItem{} = list_item) do
    ListItem.changeset(list_item, %{})
  end
end
