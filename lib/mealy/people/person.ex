defmodule Mealy.People.Person do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Teams.TeamMembership

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "people" do
    has_many :team_memberships, TeamMembership
    has_many :teams, through: [:team_memberships, :team]

    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(person, attrs) do
    person
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
