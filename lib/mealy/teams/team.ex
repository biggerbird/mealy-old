defmodule Mealy.Teams.Team do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Teams.TeamMembership
  alias Mealy.People.Person

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "teams" do
    has_many :team_memberships, TeamMembership
    has_many :teams, through: [:team_memberships, :team]
    has_many :people, through: [:team_memberships, :person]

    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(team, attrs) do
    team
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
