defmodule Mealy.Teams.TeamMembership do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Teams.Team
  alias Mealy.People.Person

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "team_memberships" do
    belongs_to :team, Team
    belongs_to :person, Person

    field :role, :string

    timestamps()
  end

  @doc false
  def changeset(team_membership, attrs) do
    team_membership
    |> cast(attrs, [:team_id, :person_id, :role])
    |> validate_required([:team_id, :person_id])
  end
end
