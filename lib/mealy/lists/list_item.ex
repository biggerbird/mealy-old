defmodule Mealy.Lists.ListItem do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Dishes.{Dish, Ingredient}
  alias Mealy.Lists.MixedList
  alias Mealy.Tags.ListItemTag

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "list_items" do
    has_many :list_item_tags, ListItemTag
    has_many :tags, through: [:list_item_tags, :tag]

    belongs_to :copied_from, __MODULE__

    belongs_to :list, MixedList
    field :order, :integer

    # generic fields
    field :label, :string
    field :note, :string

    # dish/recipe fields
    belongs_to :dish, Dish
    field :recipe_scale, :float
    field :scheduled_at, :utc_datetime

    # ingredient fields
    belongs_to :ingredient, Ingredient
    field :bought_at, :utc_datetime
    field :expires_at, :utc_datetime

    # status fields
    field :completed, :boolean
    field :completed_at, :utc_datetime
    # e.g. made, used, expired
    field :completed_verb, :string

    timestamps()
  end

  @doc false
  def changeset(list_dish, attrs) do
    list_dish
    |> cast(attrs, [:list_id, :dish_id, :order])
    |> validate_required([:list_id, :dish_id, :order])
  end
end
