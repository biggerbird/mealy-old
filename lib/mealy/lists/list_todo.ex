defmodule Mealy.Lists.ListTodo do
  use Ecto.Schema

  # import Ecto.Changeset

  alias Mealy.Lists.MixedList

  schema "list_todos" do
    belongs_to :list, MixedList

    field :name, :string
    field :order, :integer

    field :done, :boolean
  end
end
