defmodule Mealy.Lists.MixedList do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Lists.ListItem

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "lists" do
    belongs_to :parent, __MODULE__, foreign_key: :parent_id
    has_many :children, __MODULE__, foreign_key: :parent_id

    # set if team-private
    belongs_to :team, Mealy.Teams.Team
    # set if user-private
    belongs_to :person, Mealy.People.Person

    has_many :list_items, ListItem, foreign_key: :list_id
    has_many :dishes, through: [:list_items, :dish]
    has_many :ingredients, through: [:list_items, :ingredient]

    field :name, :string
    field :note, :string

    field :date, :date
    field :time, :utc_datetime

    field :order, :integer

    field :primary, :boolean, default: false
    field :default, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(list, attrs) do
    list
    |> cast(attrs, [:name, :order, :date, :time])

    # |> validate_required([:name])
  end

  def ancestors(list) do
    list = Mealy.Repo.preload(list, [:parent])
    ancestors(list.parent, [])
  end

  def ancestors(nil, ancestors), do: ancestors

  def ancestors(list, ancestors) do
    list = Mealy.Repo.preload(list, [:parent])
    ancestors(list.parent, [list | ancestors])
  end
end
