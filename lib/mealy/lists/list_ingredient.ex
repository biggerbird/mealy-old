defmodule Mealy.Lists.ListIngredient do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Dishes.Ingredient
  alias Mealy.Lists.List

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "list_ingredients" do
    belongs_to :list, List
    belongs_to :ingredient, Ingredient

    field :order, :integer

    field :completed, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(list_ingredient, attrs) do
    list_ingredient
    |> cast(attrs, [:list_id, :ingredient_id, :order, :completed])
    |> validate_required([:list_id, :ingredient_id])
  end
end
