defmodule Mealy.Recipes.RecipeStep do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Recipes.RecipeSection

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "recipe_steps" do
    belongs_to :section, RecipeSection, foreign_key: :section_id

    field :content, :string

    # for prep steps
    field :days_ahead, :integer
    field :minutes_ahead, :integer

    timestamps()
  end

  @doc false
  def changeset(recipe_step, attrs) do
    recipe_step
    |> cast(attrs, [:section_id, :content])
    |> validate_required([:section_id, :content])
  end
end
