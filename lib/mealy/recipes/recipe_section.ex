defmodule Mealy.Recipes.RecipeSection do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Dishes.Dish
  alias Mealy.Recipes.{RecipeIngredient, RecipeStep}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "recipe_sections" do
    belongs_to :dish, Dish

    has_many :ingredients, RecipeIngredient, foreign_key: :section_id
    has_many :steps, RecipeStep, foreign_key: :section_id

    field :name, :string
    field :description, :string

    timestamps()
  end

  @doc false
  def changeset(recipe_section, attrs) do
    recipe_section
    |> cast(attrs, [:dish_id, :name, :description])
    |> validate_required([:dish_id, :name])
  end
end
