defmodule Mealy.Recipes.RecipeIngredient do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Dishes.Ingredient
  alias Mealy.Recipes.RecipeSection

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "recipe_ingredients" do
    belongs_to :section, RecipeSection, foreign_key: :section_id
    belongs_to :ingredient, Ingredient

    field :quantity, :string
    field :prep, :string
    field :note, :string

    timestamps()
  end

  @doc false
  def changeset(recipe_ingredient, attrs) do
    recipe_ingredient
    |> cast(attrs, [:section_id, :ingredient_id, :quantity, :prep])
    |> validate_required([:section_id, :ingredient_id, :quantity, :prep])
  end
end
