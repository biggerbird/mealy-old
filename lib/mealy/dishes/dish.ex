# Dish does generally represent a single dish, but can also be a meal
# (whether home-cooked, restaurant, delivered, made for you, etc.)
defmodule Mealy.Dishes.Dish do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.People.Person
  alias Mealy.Recipes.RecipeSection
  alias Mealy.Teams.Team

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "dishes" do
    belongs_to :team, Team
    belongs_to :owner, Person

    belongs_to :canonical, Dish
    belongs_to :copied_from, Dish

    has_many :forks, Dish, foreign_key: :copied_from_id
    has_many :aliases, Dish, foreign_key: :canonical_id

    has_many :sections, RecipeSection

    field :name, :string
    field :description, :string
    field :yield_min, :integer
    field :yield_max, :integer
    field :yield_unit, :string

    timestamps()
  end

  @doc false
  def changeset(dish, attrs) do
    dish
    |> cast(attrs, [:name, :description, :canonical_id])
    |> validate_required([:name])
  end
end
