defmodule Mealy.Dishes.Ingredient do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Teams.Team

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "ingredients" do
    belongs_to :team, Team
    belongs_to :canonical, Ingredient

    field :name, :string
    field :plural, :string
    field :subtype, :string
    field :type, :string
    field :unit, :string
    field :unit_type, :string

    # controls default expiration date after purchasing
    field :typical_shelf_life_days, :integer

    # controls when the item appears automatically in your to-buy list
    field :target_stock_level, :integer

    # e.g. UPC code
    field :external_id, :string
    field :url, :string

    timestamps()
  end

  @doc false
  def changeset(ingredient, attrs) do
    ingredient
    |> cast(attrs, [:name, :type, :subtype, :plural, :unit, :unit_type, :canonical_id])
    |> validate_required([:name])
  end
end
