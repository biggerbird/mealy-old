defmodule Mealy.Dishes.Equipment do
  use Ecto.Schema
  import Ecto.Changeset

  alias Mealy.Teams.Team

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "equipments" do
    belongs_to :team, Team

    field :name, :string
    field :subtype, :string
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(equipment, attrs) do
    equipment
    |> cast(attrs, [:name, :type, :subtype])
    |> validate_required([:name])
  end
end
