defmodule Mealy.Recipes do
  @moduledoc """
  The Recipes context.
  """

  import Ecto.Query, warn: false
  alias Mealy.Repo

  alias Mealy.Recipes.RecipeSection

  @doc """
  Returns the list of recipe_sections.

  ## Examples

      iex> list_recipe_sections()
      [%RecipeSection{}, ...]

  """
  def list_recipe_sections do
    Repo.all(RecipeSection)
  end

  @doc """
  Gets a single recipe_section.

  Raises `Ecto.NoResultsError` if the Recipe section does not exist.

  ## Examples

      iex> get_recipe_section!(123)
      %RecipeSection{}

      iex> get_recipe_section!(456)
      ** (Ecto.NoResultsError)

  """
  def get_recipe_section!(id), do: Repo.get!(RecipeSection, id)

  @doc """
  Creates a recipe_section.

  ## Examples

      iex> create_recipe_section(%{field: value})
      {:ok, %RecipeSection{}}

      iex> create_recipe_section(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_recipe_section(attrs \\ %{}) do
    %RecipeSection{}
    |> RecipeSection.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a recipe_section.

  ## Examples

      iex> update_recipe_section(recipe_section, %{field: new_value})
      {:ok, %RecipeSection{}}

      iex> update_recipe_section(recipe_section, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_recipe_section(%RecipeSection{} = recipe_section, attrs) do
    recipe_section
    |> RecipeSection.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a RecipeSection.

  ## Examples

      iex> delete_recipe_section(recipe_section)
      {:ok, %RecipeSection{}}

      iex> delete_recipe_section(recipe_section)
      {:error, %Ecto.Changeset{}}

  """
  def delete_recipe_section(%RecipeSection{} = recipe_section) do
    Repo.delete(recipe_section)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking recipe_section changes.

  ## Examples

      iex> change_recipe_section(recipe_section)
      %Ecto.Changeset{source: %RecipeSection{}}

  """
  def change_recipe_section(%RecipeSection{} = recipe_section) do
    RecipeSection.changeset(recipe_section, %{})
  end

  alias Mealy.Recipes.RecipeIngredient

  @doc """
  Returns the list of recipe_ingredients.

  ## Examples

      iex> list_recipe_ingredients()
      [%RecipeIngredient{}, ...]

  """
  def list_recipe_ingredients do
    Repo.all(RecipeIngredient)
  end

  @doc """
  Gets a single recipe_ingredient.

  Raises `Ecto.NoResultsError` if the Recipe ingredient does not exist.

  ## Examples

      iex> get_recipe_ingredient!(123)
      %RecipeIngredient{}

      iex> get_recipe_ingredient!(456)
      ** (Ecto.NoResultsError)

  """
  def get_recipe_ingredient!(id), do: Repo.get!(RecipeIngredient, id)

  @doc """
  Creates a recipe_ingredient.

  ## Examples

      iex> create_recipe_ingredient(%{field: value})
      {:ok, %RecipeIngredient{}}

      iex> create_recipe_ingredient(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_recipe_ingredient(attrs \\ %{}) do
    %RecipeIngredient{}
    |> RecipeIngredient.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a recipe_ingredient.

  ## Examples

      iex> update_recipe_ingredient(recipe_ingredient, %{field: new_value})
      {:ok, %RecipeIngredient{}}

      iex> update_recipe_ingredient(recipe_ingredient, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_recipe_ingredient(%RecipeIngredient{} = recipe_ingredient, attrs) do
    recipe_ingredient
    |> RecipeIngredient.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a RecipeIngredient.

  ## Examples

      iex> delete_recipe_ingredient(recipe_ingredient)
      {:ok, %RecipeIngredient{}}

      iex> delete_recipe_ingredient(recipe_ingredient)
      {:error, %Ecto.Changeset{}}

  """
  def delete_recipe_ingredient(%RecipeIngredient{} = recipe_ingredient) do
    Repo.delete(recipe_ingredient)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking recipe_ingredient changes.

  ## Examples

      iex> change_recipe_ingredient(recipe_ingredient)
      %Ecto.Changeset{source: %RecipeIngredient{}}

  """
  def change_recipe_ingredient(%RecipeIngredient{} = recipe_ingredient) do
    RecipeIngredient.changeset(recipe_ingredient, %{})
  end

  alias Mealy.Recipes.RecipeStep

  @doc """
  Returns the list of recipe_steps.

  ## Examples

      iex> list_recipe_steps()
      [%RecipeStep{}, ...]

  """
  def list_recipe_steps do
    Repo.all(RecipeStep)
  end

  @doc """
  Gets a single recipe_step.

  Raises `Ecto.NoResultsError` if the Recipe step does not exist.

  ## Examples

      iex> get_recipe_step!(123)
      %RecipeStep{}

      iex> get_recipe_step!(456)
      ** (Ecto.NoResultsError)

  """
  def get_recipe_step!(id), do: Repo.get!(RecipeStep, id)

  @doc """
  Creates a recipe_step.

  ## Examples

      iex> create_recipe_step(%{field: value})
      {:ok, %RecipeStep{}}

      iex> create_recipe_step(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_recipe_step(attrs \\ %{}) do
    %RecipeStep{}
    |> RecipeStep.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a recipe_step.

  ## Examples

      iex> update_recipe_step(recipe_step, %{field: new_value})
      {:ok, %RecipeStep{}}

      iex> update_recipe_step(recipe_step, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_recipe_step(%RecipeStep{} = recipe_step, attrs) do
    recipe_step
    |> RecipeStep.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a RecipeStep.

  ## Examples

      iex> delete_recipe_step(recipe_step)
      {:ok, %RecipeStep{}}

      iex> delete_recipe_step(recipe_step)
      {:error, %Ecto.Changeset{}}

  """
  def delete_recipe_step(%RecipeStep{} = recipe_step) do
    Repo.delete(recipe_step)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking recipe_step changes.

  ## Examples

      iex> change_recipe_step(recipe_step)
      %Ecto.Changeset{source: %RecipeStep{}}

  """
  def change_recipe_step(%RecipeStep{} = recipe_step) do
    RecipeStep.changeset(recipe_step, %{})
  end
end
