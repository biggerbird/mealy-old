defmodule Mealy.Tags.Tag do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "tags" do
    belongs_to :team, Mealy.Teams.Team

    field :name, :string

    timestamps()
  end
end
