defmodule Mealy.Tags.ListItemTag do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "list_item_tags" do
    belongs_to :list_item, Mealy.Lists.ListItem
    belongs_to :tag, Mealy.Tags.Tag

    timestamps()
  end
end
