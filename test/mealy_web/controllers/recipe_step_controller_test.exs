defmodule MealyWeb.RecipeStepControllerTest do
  use MealyWeb.ConnCase

  # alias Mealy.Recipes

  # @create_attrs %{content: "some content", section_id: "7488a646-e31f-11e4-aace-600308960662"}
  # @update_attrs %{content: "some updated content", section_id: "7488a646-e31f-11e4-aace-600308960668"}
  # @invalid_attrs %{content: nil, section_id: nil}

  # def fixture(:recipe_step) do
  #   {:ok, recipe_step} = Recipes.create_recipe_step(@create_attrs)
  #   recipe_step
  # end

  # describe "index" do
  #   test "lists all recipe_steps", %{conn: conn} do
  #     conn = get(conn, Routes.recipe_step_path(conn, :index))
  #     assert html_response(conn, 200) =~ "Listing Recipe steps"
  #   end
  # end

  # describe "new recipe_step" do
  #   test "renders form", %{conn: conn} do
  #     conn = get(conn, Routes.recipe_step_path(conn, :new))
  #     assert html_response(conn, 200) =~ "New Recipe step"
  #   end
  # end

  # describe "create recipe_step" do
  #   test "redirects to show when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.recipe_step_path(conn, :create), recipe_step: @create_attrs)

  #     assert %{id: id} = redirected_params(conn)
  #     assert redirected_to(conn) == Routes.recipe_step_path(conn, :show, id)

  #     conn = get(conn, Routes.recipe_step_path(conn, :show, id))
  #     assert html_response(conn, 200) =~ "Show Recipe step"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.recipe_step_path(conn, :create), recipe_step: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "New Recipe step"
  #   end
  # end

  # describe "edit recipe_step" do
  #   setup [:create_recipe_step]

  #   test "renders form for editing chosen recipe_step", %{conn: conn, recipe_step: recipe_step} do
  #     conn = get(conn, Routes.recipe_step_path(conn, :edit, recipe_step))
  #     assert html_response(conn, 200) =~ "Edit Recipe step"
  #   end
  # end

  # describe "update recipe_step" do
  #   setup [:create_recipe_step]

  #   test "redirects when data is valid", %{conn: conn, recipe_step: recipe_step} do
  #     conn = put(conn, Routes.recipe_step_path(conn, :update, recipe_step), recipe_step: @update_attrs)
  #     assert redirected_to(conn) == Routes.recipe_step_path(conn, :show, recipe_step)

  #     conn = get(conn, Routes.recipe_step_path(conn, :show, recipe_step))
  #     assert html_response(conn, 200) =~ "some updated content"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, recipe_step: recipe_step} do
  #     conn = put(conn, Routes.recipe_step_path(conn, :update, recipe_step), recipe_step: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "Edit Recipe step"
  #   end
  # end

  # describe "delete recipe_step" do
  #   setup [:create_recipe_step]

  #   test "deletes chosen recipe_step", %{conn: conn, recipe_step: recipe_step} do
  #     conn = delete(conn, Routes.recipe_step_path(conn, :delete, recipe_step))
  #     assert redirected_to(conn) == Routes.recipe_step_path(conn, :index)
  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.recipe_step_path(conn, :show, recipe_step))
  #     end
  #   end
  # end

  # defp create_recipe_step(_) do
  #   recipe_step = fixture(:recipe_step)
  #   {:ok, recipe_step: recipe_step}
  # end
end
