defmodule MealyWeb.RecipeSectionControllerTest do
  use MealyWeb.ConnCase

  # alias Mealy.Recipes

  # @create_attrs %{description: "some description", dish_id: "7488a646-e31f-11e4-aace-600308960662", name: "some name"}
  # @update_attrs %{description: "some updated description", dish_id: "7488a646-e31f-11e4-aace-600308960668", name: "some updated name"}
  # @invalid_attrs %{description: nil, dish_id: nil, name: nil}

  # def fixture(:recipe_section) do
  #   {:ok, recipe_section} = Recipes.create_recipe_section(@create_attrs)
  #   recipe_section
  # end

  # describe "index" do
  #   test "lists all recipe_sections", %{conn: conn} do
  #     conn = get(conn, Routes.recipe_section_path(conn, :index))
  #     assert html_response(conn, 200) =~ "Listing Recipe sections"
  #   end
  # end

  # describe "new recipe_section" do
  #   test "renders form", %{conn: conn} do
  #     conn = get(conn, Routes.recipe_section_path(conn, :new))
  #     assert html_response(conn, 200) =~ "New Recipe section"
  #   end
  # end

  # describe "create recipe_section" do
  #   test "redirects to show when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.recipe_section_path(conn, :create), recipe_section: @create_attrs)

  #     assert %{id: id} = redirected_params(conn)
  #     assert redirected_to(conn) == Routes.recipe_section_path(conn, :show, id)

  #     conn = get(conn, Routes.recipe_section_path(conn, :show, id))
  #     assert html_response(conn, 200) =~ "Show Recipe section"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.recipe_section_path(conn, :create), recipe_section: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "New Recipe section"
  #   end
  # end

  # describe "edit recipe_section" do
  #   setup [:create_recipe_section]

  #   test "renders form for editing chosen recipe_section", %{conn: conn, recipe_section: recipe_section} do
  #     conn = get(conn, Routes.recipe_section_path(conn, :edit, recipe_section))
  #     assert html_response(conn, 200) =~ "Edit Recipe section"
  #   end
  # end

  # describe "update recipe_section" do
  #   setup [:create_recipe_section]

  #   test "redirects when data is valid", %{conn: conn, recipe_section: recipe_section} do
  #     conn = put(conn, Routes.recipe_section_path(conn, :update, recipe_section), recipe_section: @update_attrs)
  #     assert redirected_to(conn) == Routes.recipe_section_path(conn, :show, recipe_section)

  #     conn = get(conn, Routes.recipe_section_path(conn, :show, recipe_section))
  #     assert html_response(conn, 200) =~ "some updated description"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, recipe_section: recipe_section} do
  #     conn = put(conn, Routes.recipe_section_path(conn, :update, recipe_section), recipe_section: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "Edit Recipe section"
  #   end
  # end

  # describe "delete recipe_section" do
  #   setup [:create_recipe_section]

  #   test "deletes chosen recipe_section", %{conn: conn, recipe_section: recipe_section} do
  #     conn = delete(conn, Routes.recipe_section_path(conn, :delete, recipe_section))
  #     assert redirected_to(conn) == Routes.recipe_section_path(conn, :index)
  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.recipe_section_path(conn, :show, recipe_section))
  #     end
  #   end
  # end

  # defp create_recipe_section(_) do
  #   recipe_section = fixture(:recipe_section)
  #   {:ok, recipe_section: recipe_section}
  # end
end
