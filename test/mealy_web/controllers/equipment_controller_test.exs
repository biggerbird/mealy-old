defmodule MealyWeb.EquipmentControllerTest do
  use MealyWeb.ConnCase

  # alias Mealy.Dishes

  # @create_attrs %{name: "some name", subtype: "some subtype", type: "some type"}
  # @update_attrs %{name: "some updated name", subtype: "some updated subtype", type: "some updated type"}
  # @invalid_attrs %{name: nil, subtype: nil, type: nil}

  # def fixture(:equipment) do
  #   {:ok, equipment} = Dishes.create_equipment(@create_attrs)
  #   equipment
  # end

  # describe "index" do
  #   test "lists all equipments", %{conn: conn} do
  #     conn = get(conn, Routes.equipment_path(conn, :index))
  #     assert html_response(conn, 200) =~ "Listing Equipments"
  #   end
  # end

  # describe "new equipment" do
  #   test "renders form", %{conn: conn} do
  #     conn = get(conn, Routes.equipment_path(conn, :new))
  #     assert html_response(conn, 200) =~ "New Equipment"
  #   end
  # end

  # describe "create equipment" do
  #   test "redirects to show when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.equipment_path(conn, :create), equipment: @create_attrs)

  #     assert %{id: id} = redirected_params(conn)
  #     assert redirected_to(conn) == Routes.equipment_path(conn, :show, id)

  #     conn = get(conn, Routes.equipment_path(conn, :show, id))
  #     assert html_response(conn, 200) =~ "Show Equipment"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.equipment_path(conn, :create), equipment: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "New Equipment"
  #   end
  # end

  # describe "edit equipment" do
  #   setup [:create_equipment]

  #   test "renders form for editing chosen equipment", %{conn: conn, equipment: equipment} do
  #     conn = get(conn, Routes.equipment_path(conn, :edit, equipment))
  #     assert html_response(conn, 200) =~ "Edit Equipment"
  #   end
  # end

  # describe "update equipment" do
  #   setup [:create_equipment]

  #   test "redirects when data is valid", %{conn: conn, equipment: equipment} do
  #     conn = put(conn, Routes.equipment_path(conn, :update, equipment), equipment: @update_attrs)
  #     assert redirected_to(conn) == Routes.equipment_path(conn, :show, equipment)

  #     conn = get(conn, Routes.equipment_path(conn, :show, equipment))
  #     assert html_response(conn, 200) =~ "some updated name"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, equipment: equipment} do
  #     conn = put(conn, Routes.equipment_path(conn, :update, equipment), equipment: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "Edit Equipment"
  #   end
  # end

  # describe "delete equipment" do
  #   setup [:create_equipment]

  #   test "deletes chosen equipment", %{conn: conn, equipment: equipment} do
  #     conn = delete(conn, Routes.equipment_path(conn, :delete, equipment))
  #     assert redirected_to(conn) == Routes.equipment_path(conn, :index)
  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.equipment_path(conn, :show, equipment))
  #     end
  #   end
  # end

  # defp create_equipment(_) do
  #   equipment = fixture(:equipment)
  #   {:ok, equipment: equipment}
  # end
end
