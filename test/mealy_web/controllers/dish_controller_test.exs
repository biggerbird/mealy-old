defmodule MealyWeb.DishControllerTest do
  use MealyWeb.ConnCase

  # alias Mealy.Dishes

  # @create_attrs %{canonical_id: "7488a646-e31f-11e4-aace-600308960662", description: "some description", name: "some name"}
  # @update_attrs %{canonical_id: "7488a646-e31f-11e4-aace-600308960668", description: "some updated description", name: "some updated name"}
  # @invalid_attrs %{canonical_id: nil, description: nil, name: nil}

  # def fixture(:dish) do
  #   {:ok, dish} = Dishes.create_dish(@create_attrs)
  #   dish
  # end

  # describe "index" do
  #   test "lists all dishes", %{conn: conn} do
  #     conn = get(conn, Routes.dish_path(conn, :index))
  #     assert html_response(conn, 200) =~ "Listing Dishes"
  #   end
  # end

  # describe "new dish" do
  #   test "renders form", %{conn: conn} do
  #     conn = get(conn, Routes.dish_path(conn, :new))
  #     assert html_response(conn, 200) =~ "New Dish"
  #   end
  # end

  # describe "create dish" do
  #   test "redirects to show when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.dish_path(conn, :create), dish: @create_attrs)

  #     assert %{id: id} = redirected_params(conn)
  #     assert redirected_to(conn) == Routes.dish_path(conn, :show, id)

  #     conn = get(conn, Routes.dish_path(conn, :show, id))
  #     assert html_response(conn, 200) =~ "Show Dish"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.dish_path(conn, :create), dish: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "New Dish"
  #   end
  # end

  # describe "edit dish" do
  #   setup [:create_dish]

  #   test "renders form for editing chosen dish", %{conn: conn, dish: dish} do
  #     conn = get(conn, Routes.dish_path(conn, :edit, dish))
  #     assert html_response(conn, 200) =~ "Edit Dish"
  #   end
  # end

  # describe "update dish" do
  #   setup [:create_dish]

  #   test "redirects when data is valid", %{conn: conn, dish: dish} do
  #     conn = put(conn, Routes.dish_path(conn, :update, dish), dish: @update_attrs)
  #     assert redirected_to(conn) == Routes.dish_path(conn, :show, dish)

  #     conn = get(conn, Routes.dish_path(conn, :show, dish))
  #     assert html_response(conn, 200) =~ "some updated description"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, dish: dish} do
  #     conn = put(conn, Routes.dish_path(conn, :update, dish), dish: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "Edit Dish"
  #   end
  # end

  # describe "delete dish" do
  #   setup [:create_dish]

  #   test "deletes chosen dish", %{conn: conn, dish: dish} do
  #     conn = delete(conn, Routes.dish_path(conn, :delete, dish))
  #     assert redirected_to(conn) == Routes.dish_path(conn, :index)
  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.dish_path(conn, :show, dish))
  #     end
  #   end
  # end

  # defp create_dish(_) do
  #   dish = fixture(:dish)
  #   {:ok, dish: dish}
  # end
end
