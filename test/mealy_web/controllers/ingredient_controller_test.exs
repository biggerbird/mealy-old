defmodule MealyWeb.IngredientControllerTest do
  use MealyWeb.ConnCase

  # alias Mealy.Dishes

  # @create_attrs %{canonical_id: "some canonical_id", name: "some name", plural: "some plural", subtype: "some subtype", type: "some type", unit: "some unit", unit_type: "some unit_type"}
  # @update_attrs %{canonical_id: "some updated canonical_id", name: "some updated name", plural: "some updated plural", subtype: "some updated subtype", type: "some updated type", unit: "some updated unit", unit_type: "some updated unit_type"}
  # @invalid_attrs %{canonical_id: nil, name: nil, plural: nil, subtype: nil, type: nil, unit: nil, unit_type: nil}

  # def fixture(:ingredient) do
  #   {:ok, ingredient} = Dishes.create_ingredient(@create_attrs)
  #   ingredient
  # end

  # describe "index" do
  #   test "lists all ingredients", %{conn: conn} do
  #     conn = get(conn, Routes.ingredient_path(conn, :index))
  #     assert html_response(conn, 200) =~ "Listing Ingredients"
  #   end
  # end

  # describe "new ingredient" do
  #   test "renders form", %{conn: conn} do
  #     conn = get(conn, Routes.ingredient_path(conn, :new))
  #     assert html_response(conn, 200) =~ "New Ingredient"
  #   end
  # end

  # describe "create ingredient" do
  #   test "redirects to show when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.ingredient_path(conn, :create), ingredient: @create_attrs)

  #     assert %{id: id} = redirected_params(conn)
  #     assert redirected_to(conn) == Routes.ingredient_path(conn, :show, id)

  #     conn = get(conn, Routes.ingredient_path(conn, :show, id))
  #     assert html_response(conn, 200) =~ "Show Ingredient"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.ingredient_path(conn, :create), ingredient: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "New Ingredient"
  #   end
  # end

  # describe "edit ingredient" do
  #   setup [:create_ingredient]

  #   test "renders form for editing chosen ingredient", %{conn: conn, ingredient: ingredient} do
  #     conn = get(conn, Routes.ingredient_path(conn, :edit, ingredient))
  #     assert html_response(conn, 200) =~ "Edit Ingredient"
  #   end
  # end

  # describe "update ingredient" do
  #   setup [:create_ingredient]

  #   test "redirects when data is valid", %{conn: conn, ingredient: ingredient} do
  #     conn = put(conn, Routes.ingredient_path(conn, :update, ingredient), ingredient: @update_attrs)
  #     assert redirected_to(conn) == Routes.ingredient_path(conn, :show, ingredient)

  #     conn = get(conn, Routes.ingredient_path(conn, :show, ingredient))
  #     assert html_response(conn, 200) =~ "some updated canonical_id"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, ingredient: ingredient} do
  #     conn = put(conn, Routes.ingredient_path(conn, :update, ingredient), ingredient: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "Edit Ingredient"
  #   end
  # end

  # describe "delete ingredient" do
  #   setup [:create_ingredient]

  #   test "deletes chosen ingredient", %{conn: conn, ingredient: ingredient} do
  #     conn = delete(conn, Routes.ingredient_path(conn, :delete, ingredient))
  #     assert redirected_to(conn) == Routes.ingredient_path(conn, :index)
  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.ingredient_path(conn, :show, ingredient))
  #     end
  #   end
  # end

  # defp create_ingredient(_) do
  #   ingredient = fixture(:ingredient)
  #   {:ok, ingredient: ingredient}
  # end
end
