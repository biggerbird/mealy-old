defmodule MealyWeb.ListDishControllerTest do
  use MealyWeb.ConnCase

  # alias Mealy.Lists

  # @create_attrs %{dish_id: "7488a646-e31f-11e4-aace-600308960662", list_id: "7488a646-e31f-11e4-aace-600308960662", order: 42}
  # @update_attrs %{dish_id: "7488a646-e31f-11e4-aace-600308960668", list_id: "7488a646-e31f-11e4-aace-600308960668", order: 43}
  # @invalid_attrs %{dish_id: nil, list_id: nil, order: nil}

  # def fixture(:list_dish) do
  #   {:ok, list_dish} = Lists.create_list_dish(@create_attrs)
  #   list_dish
  # end

  # describe "index" do
  #   test "lists all list_dishes", %{conn: conn} do
  #     conn = get(conn, Routes.list_dish_path(conn, :index))
  #     assert html_response(conn, 200) =~ "Listing List dishes"
  #   end
  # end

  # describe "new list_dish" do
  #   test "renders form", %{conn: conn} do
  #     conn = get(conn, Routes.list_dish_path(conn, :new))
  #     assert html_response(conn, 200) =~ "New List dish"
  #   end
  # end

  # describe "create list_dish" do
  #   test "redirects to show when data is valid", %{conn: conn} do
  #     conn = post(conn, Routes.list_dish_path(conn, :create), list_dish: @create_attrs)

  #     assert %{id: id} = redirected_params(conn)
  #     assert redirected_to(conn) == Routes.list_dish_path(conn, :show, id)

  #     conn = get(conn, Routes.list_dish_path(conn, :show, id))
  #     assert html_response(conn, 200) =~ "Show List dish"
  #   end

  #   test "renders errors when data is invalid", %{conn: conn} do
  #     conn = post(conn, Routes.list_dish_path(conn, :create), list_dish: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "New List dish"
  #   end
  # end

  # describe "edit list_dish" do
  #   setup [:create_list_dish]

  #   test "renders form for editing chosen list_dish", %{conn: conn, list_dish: list_dish} do
  #     conn = get(conn, Routes.list_dish_path(conn, :edit, list_dish))
  #     assert html_response(conn, 200) =~ "Edit List dish"
  #   end
  # end

  # describe "update list_dish" do
  #   setup [:create_list_dish]

  #   test "redirects when data is valid", %{conn: conn, list_dish: list_dish} do
  #     conn = put(conn, Routes.list_dish_path(conn, :update, list_dish), list_dish: @update_attrs)
  #     assert redirected_to(conn) == Routes.list_dish_path(conn, :show, list_dish)

  #     conn = get(conn, Routes.list_dish_path(conn, :show, list_dish))
  #     assert html_response(conn, 200)
  #   end

  #   test "renders errors when data is invalid", %{conn: conn, list_dish: list_dish} do
  #     conn = put(conn, Routes.list_dish_path(conn, :update, list_dish), list_dish: @invalid_attrs)
  #     assert html_response(conn, 200) =~ "Edit List dish"
  #   end
  # end

  # describe "delete list_dish" do
  #   setup [:create_list_dish]

  #   test "deletes chosen list_dish", %{conn: conn, list_dish: list_dish} do
  #     conn = delete(conn, Routes.list_dish_path(conn, :delete, list_dish))
  #     assert redirected_to(conn) == Routes.list_dish_path(conn, :index)
  #     assert_error_sent 404, fn ->
  #       get(conn, Routes.list_dish_path(conn, :show, list_dish))
  #     end
  #   end
  # end

  # defp create_list_dish(_) do
  #   list_dish = fixture(:list_dish)
  #   {:ok, list_dish: list_dish}
  # end
end
