defmodule Mealy.TeamsTest do
  use Mealy.DataCase

  alias Mealy.Teams

  describe "teams" do
    alias Mealy.Teams.Team

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def team_fixture(attrs \\ %{}) do
      {:ok, team} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Teams.create_team()

      team
    end

    test "list_teams/0 returns all teams" do
      team = team_fixture()
      assert Teams.list_teams() == [team]
    end

    test "get_team!/1 returns the team with given id" do
      team = team_fixture()
      assert Teams.get_team!(team.id) == team
    end

    test "create_team/1 with valid data creates a team" do
      assert {:ok, %Team{} = team} = Teams.create_team(@valid_attrs)
      assert team.name == "some name"
    end

    test "create_team/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Teams.create_team(@invalid_attrs)
    end

    test "update_team/2 with valid data updates the team" do
      team = team_fixture()
      assert {:ok, %Team{} = team} = Teams.update_team(team, @update_attrs)
      assert team.name == "some updated name"
    end

    test "update_team/2 with invalid data returns error changeset" do
      team = team_fixture()
      assert {:error, %Ecto.Changeset{}} = Teams.update_team(team, @invalid_attrs)
      assert team == Teams.get_team!(team.id)
    end

    test "delete_team/1 deletes the team" do
      team = team_fixture()
      assert {:ok, %Team{}} = Teams.delete_team(team)
      assert_raise Ecto.NoResultsError, fn -> Teams.get_team!(team.id) end
    end

    test "change_team/1 returns a team changeset" do
      team = team_fixture()
      assert %Ecto.Changeset{} = Teams.change_team(team)
    end
  end

  describe "team_memberships" do
    alias Mealy.Teams.TeamMembership

    @valid_attrs %{person_id: "7488a646-e31f-11e4-aace-600308960662", role: "some role", team_id: "7488a646-e31f-11e4-aace-600308960662"}
    @update_attrs %{person_id: "7488a646-e31f-11e4-aace-600308960668", role: "some updated role", team_id: "7488a646-e31f-11e4-aace-600308960668"}
    @invalid_attrs %{person_id: nil, role: nil, team_id: nil}

    def team_membership_fixture(attrs \\ %{}) do
      {:ok, team_membership} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Teams.create_team_membership()

      team_membership
    end

    test "list_team_memberships/0 returns all team_memberships" do
      team_membership = team_membership_fixture()
      assert Teams.list_team_memberships() == [team_membership]
    end

    test "get_team_membership!/1 returns the team_membership with given id" do
      team_membership = team_membership_fixture()
      assert Teams.get_team_membership!(team_membership.id) == team_membership
    end

    test "create_team_membership/1 with valid data creates a team_membership" do
      assert {:ok, %TeamMembership{} = team_membership} = Teams.create_team_membership(@valid_attrs)
      assert team_membership.person_id == "7488a646-e31f-11e4-aace-600308960662"
      assert team_membership.role == "some role"
      assert team_membership.team_id == "7488a646-e31f-11e4-aace-600308960662"
    end

    test "create_team_membership/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Teams.create_team_membership(@invalid_attrs)
    end

    test "update_team_membership/2 with valid data updates the team_membership" do
      team_membership = team_membership_fixture()
      assert {:ok, %TeamMembership{} = team_membership} = Teams.update_team_membership(team_membership, @update_attrs)
      assert team_membership.person_id == "7488a646-e31f-11e4-aace-600308960668"
      assert team_membership.role == "some updated role"
      assert team_membership.team_id == "7488a646-e31f-11e4-aace-600308960668"
    end

    test "update_team_membership/2 with invalid data returns error changeset" do
      team_membership = team_membership_fixture()
      assert {:error, %Ecto.Changeset{}} = Teams.update_team_membership(team_membership, @invalid_attrs)
      assert team_membership == Teams.get_team_membership!(team_membership.id)
    end

    test "delete_team_membership/1 deletes the team_membership" do
      team_membership = team_membership_fixture()
      assert {:ok, %TeamMembership{}} = Teams.delete_team_membership(team_membership)
      assert_raise Ecto.NoResultsError, fn -> Teams.get_team_membership!(team_membership.id) end
    end

    test "change_team_membership/1 returns a team_membership changeset" do
      team_membership = team_membership_fixture()
      assert %Ecto.Changeset{} = Teams.change_team_membership(team_membership)
    end
  end
end
