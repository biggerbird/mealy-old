defmodule Mealy.ListsTest do
  use Mealy.DataCase

  alias Mealy.Lists

  describe "lists" do
    #   alias Mealy.Lists.MixedList

    @valid_attrs %{name: "some name", order: 1, default: false, primary: false}
    #   @update_attrs %{date: ~N[2011-05-18 15:01:01], name: "some updated name", order: 43, time: "2011-05-18T15:01:01Z"}
    #   @invalid_attrs %{date: nil, name: nil, order: nil, time: nil}

    def list_fixture(attrs \\ %{}) do
      {:ok, list} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Lists.create_list()

      list
    end

    test "list_lists/0 returns all lists" do
      list = list_fixture()
      assert Lists.list_lists() == [list]
    end

    test "get_list!/1 returns the list with given id" do
      list = list_fixture()
      assert Lists.get_list!(list.id) == list
    end

    #   test "create_list/1 with valid data creates a list" do
    #     assert {:ok, %MixedList{} = list} = Lists.create_list(@valid_attrs)
    #     assert list.date == ~N[2010-04-17 14:00:00]
    #     assert list.name == "some name"
    #     assert list.order == 42
    #     assert list.time == DateTime.from_naive!(~N[2010-04-17T14:00:00Z], "Etc/UTC")
    #   end

    #   test "create_list/1 with invalid data returns error changeset" do
    #     assert {:error, %Ecto.Changeset{}} = Lists.create_list(@invalid_attrs)
    #   end

    #   test "update_list/2 with valid data updates the list" do
    #     list = list_fixture()
    #     assert {:ok, %MixedList{} = list} = Lists.update_list(list, @update_attrs)
    #     assert list.date == ~N[2011-05-18 15:01:01]
    #     assert list.name == "some updated name"
    #     assert list.order == 43
    #     assert list.time == DateTime.from_naive!(~N[2011-05-18T15:01:01Z], "Etc/UTC")
    #   end

    #   test "update_list/2 with invalid data returns error changeset" do
    #     list = list_fixture()
    #     assert {:error, %Ecto.Changeset{}} = Lists.update_list(list, @invalid_attrs)
    #     assert list == Lists.get_list!(list.id)
    #   end

    #   test "delete_list/1 deletes the list" do
    #     list = list_fixture()
    #     assert {:ok, %MixedList{}} = Lists.delete_list(list)
    #     assert_raise Ecto.NoResultsError, fn -> Lists.get_list!(list.id) end
    #   end

    #   test "change_list/1 returns a list changeset" do
    #     list = list_fixture()
    #     assert %Ecto.Changeset{} = Lists.change_list(list)
    #   end
  end

  # describe "list_items" do
  #   alias Mealy.Lists.ListItem

  #   @valid_attrs %{dish_id: "7488a646-e31f-11e4-aace-600308960662", list_id: "7488a646-e31f-11e4-aace-600308960662", order: 42}
  #   @update_attrs %{dish_id: "7488a646-e31f-11e4-aace-600308960668", list_id: "7488a646-e31f-11e4-aace-600308960668", order: 43}
  #   @invalid_attrs %{dish_id: nil, list_id: nil, order: nil}

  #   def list_dish_fixture(attrs \\ %{}) do
  #     {:ok, list_dish} =
  #       attrs
  #       |> Enum.into(@valid_attrs)
  #       |> Lists.create_list_dish()

  #     list_dish
  #   end

  #   test "list_list_dishes/0 returns all list_dishes" do
  #     list_dish = list_dish_fixture()
  #     assert Lists.list_list_dishes() == [list_dish]
  #   end

  #   test "get_list_dish!/1 returns the list_dish with given id" do
  #     list_dish = list_dish_fixture()
  #     assert Lists.get_list_dish!(list_dish.id) == list_dish
  #   end

  #   test "create_list_dish/1 with valid data creates a list_dish" do
  #     assert {:ok, %MixedListDish{} = list_dish} = Lists.create_list_dish(@valid_attrs)
  #     assert list_dish.dish_id == "7488a646-e31f-11e4-aace-600308960662"
  #     assert list_dish.list_id == "7488a646-e31f-11e4-aace-600308960662"
  #     assert list_dish.order == 42
  #   end

  #   test "create_list_dish/1 with invalid data returns error changeset" do
  #     assert {:error, %Ecto.Changeset{}} = Lists.create_list_dish(@invalid_attrs)
  #   end

  #   test "update_list_dish/2 with valid data updates the list_dish" do
  #     list_dish = list_dish_fixture()
  #     assert {:ok, %MixedListDish{} = list_dish} = Lists.update_list_dish(list_dish, @update_attrs)
  #     assert list_dish.dish_id == "7488a646-e31f-11e4-aace-600308960668"
  #     assert list_dish.list_id == "7488a646-e31f-11e4-aace-600308960668"
  #     assert list_dish.order == 43
  #   end

  #   test "update_list_dish/2 with invalid data returns error changeset" do
  #     list_dish = list_dish_fixture()
  #     assert {:error, %Ecto.Changeset{}} = Lists.update_list_dish(list_dish, @invalid_attrs)
  #     assert list_dish == Lists.get_list_dish!(list_dish.id)
  #   end

  #   test "delete_list_dish/1 deletes the list_dish" do
  #     list_dish = list_dish_fixture()
  #     assert {:ok, %MixedListDish{}} = Lists.delete_list_dish(list_dish)
  #     assert_raise Ecto.NoResultsError, fn -> Lists.get_list_dish!(list_dish.id) end
  #   end

  #   test "change_list_dish/1 returns a list_dish changeset" do
  #     list_dish = list_dish_fixture()
  #     assert %Ecto.Changeset{} = Lists.change_list_dish(list_dish)
  #   end
  # end

  # describe "list_ingredients" do
  #   alias Mealy.Lists.ListIngredient

  #   @valid_attrs %{completed: true, ingredient_id: "7488a646-e31f-11e4-aace-600308960662", list_id: "7488a646-e31f-11e4-aace-600308960662", order: 42}
  #   @update_attrs %{completed: false, ingredient_id: "7488a646-e31f-11e4-aace-600308960668", list_id: "7488a646-e31f-11e4-aace-600308960668", order: 43}
  #   @invalid_attrs %{completed: nil, ingredient_id: nil, list_id: nil, order: nil}

  #   def list_ingredient_fixture(attrs \\ %{}) do
  #     {:ok, list_ingredient} =
  #       attrs
  #       |> Enum.into(@valid_attrs)
  #       |> Lists.create_list_ingredient()

  #     list_ingredient
  #   end

  #   test "list_list_ingredients/0 returns all list_ingredients" do
  #     list_ingredient = list_ingredient_fixture()
  #     assert Lists.list_list_ingredients() == [list_ingredient]
  #   end

  #   test "get_list_ingredient!/1 returns the list_ingredient with given id" do
  #     list_ingredient = list_ingredient_fixture()
  #     assert Lists.get_list_ingredient!(list_ingredient.id) == list_ingredient
  #   end

  #   test "create_list_ingredient/1 with valid data creates a list_ingredient" do
  #     assert {:ok, %MixedListIngredient{} = list_ingredient} = Lists.create_list_ingredient(@valid_attrs)
  #     assert list_ingredient.completed == true
  #     assert list_ingredient.ingredient_id == "7488a646-e31f-11e4-aace-600308960662"
  #     assert list_ingredient.list_id == "7488a646-e31f-11e4-aace-600308960662"
  #     assert list_ingredient.order == 42
  #   end

  #   test "create_list_ingredient/1 with invalid data returns error changeset" do
  #     assert {:error, %Ecto.Changeset{}} = Lists.create_list_ingredient(@invalid_attrs)
  #   end

  #   test "update_list_ingredient/2 with valid data updates the list_ingredient" do
  #     list_ingredient = list_ingredient_fixture()
  #     assert {:ok, %MixedListIngredient{} = list_ingredient} = Lists.update_list_ingredient(list_ingredient, @update_attrs)
  #     assert list_ingredient.completed == false
  #     assert list_ingredient.ingredient_id == "7488a646-e31f-11e4-aace-600308960668"
  #     assert list_ingredient.list_id == "7488a646-e31f-11e4-aace-600308960668"
  #     assert list_ingredient.order == 43
  #   end

  #   test "update_list_ingredient/2 with invalid data returns error changeset" do
  #     list_ingredient = list_ingredient_fixture()
  #     assert {:error, %Ecto.Changeset{}} = Lists.update_list_ingredient(list_ingredient, @invalid_attrs)
  #     assert list_ingredient == Lists.get_list_ingredient!(list_ingredient.id)
  #   end

  #   test "delete_list_ingredient/1 deletes the list_ingredient" do
  #     list_ingredient = list_ingredient_fixture()
  #     assert {:ok, %MixedListIngredient{}} = Lists.delete_list_ingredient(list_ingredient)
  #     assert_raise Ecto.NoResultsError, fn -> Lists.get_list_ingredient!(list_ingredient.id) end
  #   end

  #   test "change_list_ingredient/1 returns a list_ingredient changeset" do
  #     list_ingredient = list_ingredient_fixture()
  #     assert %Ecto.Changeset{} = Lists.change_list_ingredient(list_ingredient)
  #   end
  # end
end
