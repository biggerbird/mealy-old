defmodule Mealy.DishesTest do
  use Mealy.DataCase

  alias Mealy.Dishes

  describe "equipments" do
    # alias Mealy.Dishes.Equipment

    @valid_attrs %{name: "some name", subtype: "some subtype", type: "some type"}
    # @update_attrs %{
    #   name: "some updated name",
    #   subtype: "some updated subtype",
    #   type: "some updated type"
    # }
    # @invalid_attrs %{name: nil, subtype: nil, type: nil}

    def equipment_fixture(attrs \\ %{}) do
      {:ok, equipment} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Dishes.create_equipment()

      equipment
    end

    test "list_equipments/0 returns all equipments" do
      equipment = equipment_fixture()
      assert Dishes.list_equipments() == [equipment]
    end

    test "get_equipment!/1 returns the equipment with given id" do
      equipment = equipment_fixture()
      assert Dishes.get_equipment!(equipment.id) == equipment
    end

    #   test "create_equipment/1 with valid data creates a equipment" do
    #     assert {:ok, %Equipment{} = equipment} = Dishes.create_equipment(@valid_attrs)
    #     assert equipment.name == "some name"
    #     assert equipment.subtype == "some subtype"
    #     assert equipment.type == "some type"
    #   end

    #   test "create_equipment/1 with invalid data returns error changeset" do
    #     assert {:error, %Ecto.Changeset{}} = Dishes.create_equipment(@invalid_attrs)
    #   end

    #   test "update_equipment/2 with valid data updates the equipment" do
    #     equipment = equipment_fixture()
    #     assert {:ok, %Equipment{} = equipment} = Dishes.update_equipment(equipment, @update_attrs)
    #     assert equipment.name == "some updated name"
    #     assert equipment.subtype == "some updated subtype"
    #     assert equipment.type == "some updated type"
    #   end

    #   test "update_equipment/2 with invalid data returns error changeset" do
    #     equipment = equipment_fixture()
    #     assert {:error, %Ecto.Changeset{}} = Dishes.update_equipment(equipment, @invalid_attrs)
    #     assert equipment == Dishes.get_equipment!(equipment.id)
    #   end

    #   test "delete_equipment/1 deletes the equipment" do
    #     equipment = equipment_fixture()
    #     assert {:ok, %Equipment{}} = Dishes.delete_equipment(equipment)
    #     assert_raise Ecto.NoResultsError, fn -> Dishes.get_equipment!(equipment.id) end
    #   end

    #   test "change_equipment/1 returns a equipment changeset" do
    #     equipment = equipment_fixture()
    #     assert %Ecto.Changeset{} = Dishes.change_equipment(equipment)
    #   end
  end

  describe "ingredients" do
    # alias Mealy.Dishes.Ingredient

    @valid_attrs %{
      # canonical_id: "some canonical_id",
      name: "some name",
      plural: "some plural",
      subtype: "some subtype",
      type: "some type",
      unit: "some unit",
      unit_type: "some unit_type"
    }
    # @update_attrs %{
    #   # canonical_id: "some updated canonical_id",
    #   name: "some updated name",
    #   plural: "some updated plural",
    #   subtype: "some updated subtype",
    #   type: "some updated type",
    #   unit: "some updated unit",
    #   unit_type: "some updated unit_type"
    # }
    # @invalid_attrs %{
    #   # canonical_id: nil,
    #   name: nil,
    #   plural: nil,
    #   subtype: nil,
    #   type: nil,
    #   unit: nil,
    #   unit_type: nil
    # }

    def ingredient_fixture(attrs \\ %{}) do
      {:ok, ingredient} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Dishes.create_ingredient()

      ingredient
    end

    test "list_ingredients/0 returns all ingredients" do
      ingredient = ingredient_fixture()
      assert Dishes.list_ingredients() == [ingredient]
    end

    test "get_ingredient!/1 returns the ingredient with given id" do
      ingredient = ingredient_fixture()
      assert Dishes.get_ingredient!(ingredient.id) == ingredient
    end

    #   test "create_ingredient/1 with valid data creates a ingredient" do
    #     assert {:ok, %Ingredient{} = ingredient} = Dishes.create_ingredient(@valid_attrs)
    #     assert ingredient.canonical_id == "some canonical_id"
    #     assert ingredient.name == "some name"
    #     assert ingredient.plural == "some plural"
    #     assert ingredient.subtype == "some subtype"
    #     assert ingredient.type == "some type"
    #     assert ingredient.unit == "some unit"
    #     assert ingredient.unit_type == "some unit_type"
    #   end

    #   test "create_ingredient/1 with invalid data returns error changeset" do
    #     assert {:error, %Ecto.Changeset{}} = Dishes.create_ingredient(@invalid_attrs)
    #   end

    #   test "update_ingredient/2 with valid data updates the ingredient" do
    #     ingredient = ingredient_fixture()
    #     assert {:ok, %Ingredient{} = ingredient} = Dishes.update_ingredient(ingredient, @update_attrs)
    #     assert ingredient.canonical_id == "some updated canonical_id"
    #     assert ingredient.name == "some updated name"
    #     assert ingredient.plural == "some updated plural"
    #     assert ingredient.subtype == "some updated subtype"
    #     assert ingredient.type == "some updated type"
    #     assert ingredient.unit == "some updated unit"
    #     assert ingredient.unit_type == "some updated unit_type"
    #   end

    #   test "update_ingredient/2 with invalid data returns error changeset" do
    #     ingredient = ingredient_fixture()
    #     assert {:error, %Ecto.Changeset{}} = Dishes.update_ingredient(ingredient, @invalid_attrs)
    #     assert ingredient == Dishes.get_ingredient!(ingredient.id)
    #   end

    #   test "delete_ingredient/1 deletes the ingredient" do
    #     ingredient = ingredient_fixture()
    #     assert {:ok, %Ingredient{}} = Dishes.delete_ingredient(ingredient)
    #     assert_raise Ecto.NoResultsError, fn -> Dishes.get_ingredient!(ingredient.id) end
    #   end

    #   test "change_ingredient/1 returns a ingredient changeset" do
    #     ingredient = ingredient_fixture()
    #     assert %Ecto.Changeset{} = Dishes.change_ingredient(ingredient)
    #   end
  end

  # describe "dishes" do
  #   alias Mealy.Dishes.Dish

  #   @valid_attrs %{description: "some description", name: "some name"}
  #   @update_attrs %{description: "some updated description", name: "some updated name"}
  #   @invalid_attrs %{description: nil, name: nil}

  #   def dish_fixture(attrs \\ %{}) do
  #     {:ok, dish} =
  #       attrs
  #       |> Enum.into(@valid_attrs)
  #       |> Dishes.create_dish()

  #     dish
  #   end

  #   test "list_dishes/0 returns all dishes" do
  #     dish = dish_fixture()
  #     assert Dishes.list_dishes() == [dish]
  #   end

  #   test "get_dish!/1 returns the dish with given id" do
  #     dish = dish_fixture()
  #     assert Dishes.get_dish!(dish.id) == dish
  #   end

  #   test "create_dish/1 with valid data creates a dish" do
  #     assert {:ok, %Dish{} = dish} = Dishes.create_dish(@valid_attrs)
  #     assert dish.description == "some description"
  #     assert dish.name == "some name"
  #   end

  #   test "create_dish/1 with invalid data returns error changeset" do
  #     assert {:error, %Ecto.Changeset{}} = Dishes.create_dish(@invalid_attrs)
  #   end

  #   test "update_dish/2 with valid data updates the dish" do
  #     dish = dish_fixture()
  #     assert {:ok, %Dish{} = dish} = Dishes.update_dish(dish, @update_attrs)
  #     assert dish.description == "some updated description"
  #     assert dish.name == "some updated name"
  #   end

  #   test "update_dish/2 with invalid data returns error changeset" do
  #     dish = dish_fixture()
  #     assert {:error, %Ecto.Changeset{}} = Dishes.update_dish(dish, @invalid_attrs)
  #     assert dish == Dishes.get_dish!(dish.id)
  #   end

  #   test "delete_dish/1 deletes the dish" do
  #     dish = dish_fixture()
  #     assert {:ok, %Dish{}} = Dishes.delete_dish(dish)
  #     assert_raise Ecto.NoResultsError, fn -> Dishes.get_dish!(dish.id) end
  #   end

  #   test "change_dish/1 returns a dish changeset" do
  #     dish = dish_fixture()
  #     assert %Ecto.Changeset{} = Dishes.change_dish(dish)
  #   end
  # end

  # describe "dish_makers" do
  #   alias Mealy.Dishes.DishMaker

  #   @valid_attrs %{dish_id: "7488a646-e31f-11e4-aace-600308960662", person_id: "7488a646-e31f-11e4-aace-600308960662"}
  #   @update_attrs %{dish_id: "7488a646-e31f-11e4-aace-600308960668", person_id: "7488a646-e31f-11e4-aace-600308960668"}
  #   @invalid_attrs %{dish_id: nil, person_id: nil}

  #   def dish_maker_fixture(attrs \\ %{}) do
  #     {:ok, dish_maker} =
  #       attrs
  #       |> Enum.into(@valid_attrs)
  #       |> Dishes.create_dish_maker()

  #     dish_maker
  #   end

  #   test "list_dish_makers/0 returns all dish_makers" do
  #     dish_maker = dish_maker_fixture()
  #     assert Dishes.list_dish_makers() == [dish_maker]
  #   end

  #   test "get_dish_maker!/1 returns the dish_maker with given id" do
  #     dish_maker = dish_maker_fixture()
  #     assert Dishes.get_dish_maker!(dish_maker.id) == dish_maker
  #   end

  #   test "create_dish_maker/1 with valid data creates a dish_maker" do
  #     assert {:ok, %DishMaker{} = dish_maker} = Dishes.create_dish_maker(@valid_attrs)
  #     assert dish_maker.dish_id == "7488a646-e31f-11e4-aace-600308960662"
  #     assert dish_maker.person_id == "7488a646-e31f-11e4-aace-600308960662"
  #   end

  #   test "create_dish_maker/1 with invalid data returns error changeset" do
  #     assert {:error, %Ecto.Changeset{}} = Dishes.create_dish_maker(@invalid_attrs)
  #   end

  #   test "update_dish_maker/2 with valid data updates the dish_maker" do
  #     dish_maker = dish_maker_fixture()
  #     assert {:ok, %DishMaker{} = dish_maker} = Dishes.update_dish_maker(dish_maker, @update_attrs)
  #     assert dish_maker.dish_id == "7488a646-e31f-11e4-aace-600308960668"
  #     assert dish_maker.person_id == "7488a646-e31f-11e4-aace-600308960668"
  #   end

  #   test "update_dish_maker/2 with invalid data returns error changeset" do
  #     dish_maker = dish_maker_fixture()
  #     assert {:error, %Ecto.Changeset{}} = Dishes.update_dish_maker(dish_maker, @invalid_attrs)
  #     assert dish_maker == Dishes.get_dish_maker!(dish_maker.id)
  #   end

  #   test "delete_dish_maker/1 deletes the dish_maker" do
  #     dish_maker = dish_maker_fixture()
  #     assert {:ok, %DishMaker{}} = Dishes.delete_dish_maker(dish_maker)
  #     assert_raise Ecto.NoResultsError, fn -> Dishes.get_dish_maker!(dish_maker.id) end
  #   end

  #   test "change_dish_maker/1 returns a dish_maker changeset" do
  #     dish_maker = dish_maker_fixture()
  #     assert %Ecto.Changeset{} = Dishes.change_dish_maker(dish_maker)
  #   end
  # end

  # describe "dishes" do
  #   alias Mealy.Dishes.Dish

  #   @valid_attrs %{canonical_id: "7488a646-e31f-11e4-aace-600308960662", description: "some description", name: "some name"}
  #   @update_attrs %{canonical_id: "7488a646-e31f-11e4-aace-600308960668", description: "some updated description", name: "some updated name"}
  #   @invalid_attrs %{canonical_id: nil, description: nil, name: nil}

  #   def dish_fixture(attrs \\ %{}) do
  #     {:ok, dish} =
  #       attrs
  #       |> Enum.into(@valid_attrs)
  #       |> Dishes.create_dish()

  #     dish
  #   end

  #   test "list_dishes/0 returns all dishes" do
  #     dish = dish_fixture()
  #     assert Dishes.list_dishes() == [dish]
  #   end

  #   test "get_dish!/1 returns the dish with given id" do
  #     dish = dish_fixture()
  #     assert Dishes.get_dish!(dish.id) == dish
  #   end

  #   test "create_dish/1 with valid data creates a dish" do
  #     assert {:ok, %Dish{} = dish} = Dishes.create_dish(@valid_attrs)
  #     assert dish.canonical_id == "7488a646-e31f-11e4-aace-600308960662"
  #     assert dish.description == "some description"
  #     assert dish.name == "some name"
  #   end

  #   test "create_dish/1 with invalid data returns error changeset" do
  #     assert {:error, %Ecto.Changeset{}} = Dishes.create_dish(@invalid_attrs)
  #   end

  #   test "update_dish/2 with valid data updates the dish" do
  #     dish = dish_fixture()
  #     assert {:ok, %Dish{} = dish} = Dishes.update_dish(dish, @update_attrs)
  #     assert dish.canonical_id == "7488a646-e31f-11e4-aace-600308960668"
  #     assert dish.description == "some updated description"
  #     assert dish.name == "some updated name"
  #   end

  #   test "update_dish/2 with invalid data returns error changeset" do
  #     dish = dish_fixture()
  #     assert {:error, %Ecto.Changeset{}} = Dishes.update_dish(dish, @invalid_attrs)
  #     assert dish == Dishes.get_dish!(dish.id)
  #   end

  #   test "delete_dish/1 deletes the dish" do
  #     dish = dish_fixture()
  #     assert {:ok, %Dish{}} = Dishes.delete_dish(dish)
  #     assert_raise Ecto.NoResultsError, fn -> Dishes.get_dish!(dish.id) end
  #   end

  #   test "change_dish/1 returns a dish changeset" do
  #     dish = dish_fixture()
  #     assert %Ecto.Changeset{} = Dishes.change_dish(dish)
  #   end
  # end
end
