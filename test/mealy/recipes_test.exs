defmodule Mealy.RecipesTest do
  use Mealy.DataCase

  alias Mealy.Recipes

  describe "recipe_sections" do
    #   alias Mealy.Recipes.RecipeSection

    @valid_attrs %{
      description: "some description",
      dish_id: "7488a646-e31f-11e4-aace-600308960662",
      name: "some name"
    }
    #   @update_attrs %{description: "some updated description", dish_id: "7488a646-e31f-11e4-aace-600308960668", name: "some updated name"}
    #   @invalid_attrs %{description: nil, dish_id: nil, name: nil}

    def recipe_section_fixture(attrs \\ %{}) do
      {:ok, recipe_section} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Recipes.create_recipe_section()

      recipe_section
    end

    test "list_recipe_sections/0 returns all recipe_sections" do
      recipe_section = recipe_section_fixture()
      assert Recipes.list_recipe_sections() == [recipe_section]
    end

    test "get_recipe_section!/1 returns the recipe_section with given id" do
      recipe_section = recipe_section_fixture()
      assert Recipes.get_recipe_section!(recipe_section.id) == recipe_section
    end

    #   test "create_recipe_section/1 with valid data creates a recipe_section" do
    #     assert {:ok, %RecipeSection{} = recipe_section} = Recipes.create_recipe_section(@valid_attrs)
    #     assert recipe_section.description == "some description"
    #     assert recipe_section.dish_id == "7488a646-e31f-11e4-aace-600308960662"
    #     assert recipe_section.name == "some name"
    #   end

    #   test "create_recipe_section/1 with invalid data returns error changeset" do
    #     assert {:error, %Ecto.Changeset{}} = Recipes.create_recipe_section(@invalid_attrs)
    #   end

    #   test "update_recipe_section/2 with valid data updates the recipe_section" do
    #     recipe_section = recipe_section_fixture()
    #     assert {:ok, %RecipeSection{} = recipe_section} = Recipes.update_recipe_section(recipe_section, @update_attrs)
    #     assert recipe_section.description == "some updated description"
    #     assert recipe_section.dish_id == "7488a646-e31f-11e4-aace-600308960668"
    #     assert recipe_section.name == "some updated name"
    #   end

    #   test "update_recipe_section/2 with invalid data returns error changeset" do
    #     recipe_section = recipe_section_fixture()
    #     assert {:error, %Ecto.Changeset{}} = Recipes.update_recipe_section(recipe_section, @invalid_attrs)
    #     assert recipe_section == Recipes.get_recipe_section!(recipe_section.id)
    #   end

    #   test "delete_recipe_section/1 deletes the recipe_section" do
    #     recipe_section = recipe_section_fixture()
    #     assert {:ok, %RecipeSection{}} = Recipes.delete_recipe_section(recipe_section)
    #     assert_raise Ecto.NoResultsError, fn -> Recipes.get_recipe_section!(recipe_section.id) end
    #   end

    #   test "change_recipe_section/1 returns a recipe_section changeset" do
    #     recipe_section = recipe_section_fixture()
    #     assert %Ecto.Changeset{} = Recipes.change_recipe_section(recipe_section)
    #   end
  end

  describe "recipe_ingredients" do
    #   alias Mealy.Recipes.RecipeIngredient

    @valid_attrs %{
      section_id: "7488a646-e31f-11e4-aace-600308960662",
      dish_id: "7488a646-e31f-11e4-aace-600308960662",
      ingredient_id: "7488a646-e31f-11e4-aace-600308960662",
      prep: "some prep",
      quantity: "some quantity",
      note: "some note"
    }
    #   @update_attrs %{dish_id: "7488a646-e31f-11e4-aace-600308960668", ingredient_id: "7488a646-e31f-11e4-aace-600308960668", prep: "some updated prep", quantity: "some updated quantity"}
    #   @invalid_attrs %{dish_id: nil, ingredient_id: nil, prep: nil, quantity: nil}

    def recipe_ingredient_fixture(attrs \\ %{}) do
      {:ok, recipe_ingredient} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Recipes.create_recipe_ingredient()

      recipe_ingredient
    end

    test "list_recipe_ingredients/0 returns all recipe_ingredients" do
      recipe_ingredient = recipe_ingredient_fixture()
      assert Recipes.list_recipe_ingredients() == [recipe_ingredient]
    end

    test "get_recipe_ingredient!/1 returns the recipe_ingredient with given id" do
      recipe_ingredient = recipe_ingredient_fixture()
      assert Recipes.get_recipe_ingredient!(recipe_ingredient.id) == recipe_ingredient
    end

    #   test "create_recipe_ingredient/1 with valid data creates a recipe_ingredient" do
    #     assert {:ok, %RecipeIngredient{} = recipe_ingredient} = Recipes.create_recipe_ingredient(@valid_attrs)
    #     assert recipe_ingredient.dish_id == "7488a646-e31f-11e4-aace-600308960662"
    #     assert recipe_ingredient.ingredient_id == "7488a646-e31f-11e4-aace-600308960662"
    #     assert recipe_ingredient.prep == "some prep"
    #     assert recipe_ingredient.quantity == "some quantity"
    #   end

    #   test "create_recipe_ingredient/1 with invalid data returns error changeset" do
    #     assert {:error, %Ecto.Changeset{}} = Recipes.create_recipe_ingredient(@invalid_attrs)
    #   end

    #   test "update_recipe_ingredient/2 with valid data updates the recipe_ingredient" do
    #     recipe_ingredient = recipe_ingredient_fixture()
    #     assert {:ok, %RecipeIngredient{} = recipe_ingredient} = Recipes.update_recipe_ingredient(recipe_ingredient, @update_attrs)
    #     assert recipe_ingredient.dish_id == "7488a646-e31f-11e4-aace-600308960668"
    #     assert recipe_ingredient.ingredient_id == "7488a646-e31f-11e4-aace-600308960668"
    #     assert recipe_ingredient.prep == "some updated prep"
    #     assert recipe_ingredient.quantity == "some updated quantity"
    #   end

    #   test "update_recipe_ingredient/2 with invalid data returns error changeset" do
    #     recipe_ingredient = recipe_ingredient_fixture()
    #     assert {:error, %Ecto.Changeset{}} = Recipes.update_recipe_ingredient(recipe_ingredient, @invalid_attrs)
    #     assert recipe_ingredient == Recipes.get_recipe_ingredient!(recipe_ingredient.id)
    #   end

    #   test "delete_recipe_ingredient/1 deletes the recipe_ingredient" do
    #     recipe_ingredient = recipe_ingredient_fixture()
    #     assert {:ok, %RecipeIngredient{}} = Recipes.delete_recipe_ingredient(recipe_ingredient)
    #     assert_raise Ecto.NoResultsError, fn -> Recipes.get_recipe_ingredient!(recipe_ingredient.id) end
    #   end

    #   test "change_recipe_ingredient/1 returns a recipe_ingredient changeset" do
    #     recipe_ingredient = recipe_ingredient_fixture()
    #     assert %Ecto.Changeset{} = Recipes.change_recipe_ingredient(recipe_ingredient)
    #   end
  end

  describe "recipe_steps" do
    #   alias Mealy.Recipes.RecipeStep

    @valid_attrs %{
      content: "some content",
      section_id: "7488a646-e31f-11e4-aace-600308960662"
    }
    #   @update_attrs %{content: "some updated content", section_id: "7488a646-e31f-11e4-aace-600308960668"}
    #   @invalid_attrs %{content: nil, section_id: nil}

    def recipe_step_fixture(attrs \\ %{}) do
      {:ok, recipe_step} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Recipes.create_recipe_step()

      recipe_step
    end

    test "list_recipe_steps/0 returns all recipe_steps" do
      recipe_step = recipe_step_fixture()
      assert Recipes.list_recipe_steps() == [recipe_step]
    end

    test "get_recipe_step!/1 returns the recipe_step with given id" do
      recipe_step = recipe_step_fixture()
      assert Recipes.get_recipe_step!(recipe_step.id) == recipe_step
    end

    #   test "create_recipe_step/1 with valid data creates a recipe_step" do
    #     assert {:ok, %RecipeStep{} = recipe_step} = Recipes.create_recipe_step(@valid_attrs)
    #     assert recipe_step.content == "some content"
    #     assert recipe_step.section_id == "7488a646-e31f-11e4-aace-600308960662"
    #   end

    #   test "create_recipe_step/1 with invalid data returns error changeset" do
    #     assert {:error, %Ecto.Changeset{}} = Recipes.create_recipe_step(@invalid_attrs)
    #   end

    #   test "update_recipe_step/2 with valid data updates the recipe_step" do
    #     recipe_step = recipe_step_fixture()
    #     assert {:ok, %RecipeStep{} = recipe_step} = Recipes.update_recipe_step(recipe_step, @update_attrs)
    #     assert recipe_step.content == "some updated content"
    #     assert recipe_step.section_id == "7488a646-e31f-11e4-aace-600308960668"
    #   end

    #   test "update_recipe_step/2 with invalid data returns error changeset" do
    #     recipe_step = recipe_step_fixture()
    #     assert {:error, %Ecto.Changeset{}} = Recipes.update_recipe_step(recipe_step, @invalid_attrs)
    #     assert recipe_step == Recipes.get_recipe_step!(recipe_step.id)
    #   end

    #   test "delete_recipe_step/1 deletes the recipe_step" do
    #     recipe_step = recipe_step_fixture()
    #     assert {:ok, %RecipeStep{}} = Recipes.delete_recipe_step(recipe_step)
    #     assert_raise Ecto.NoResultsError, fn -> Recipes.get_recipe_step!(recipe_step.id) end
    #   end

    #   test "change_recipe_step/1 returns a recipe_step changeset" do
    #     recipe_step = recipe_step_fixture()
    #     assert %Ecto.Changeset{} = Recipes.change_recipe_step(recipe_step)
    #   end
  end
end
