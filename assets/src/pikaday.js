/* global $ */
import Moment from "moment";
import Pikaday from "pikaday";

window.pikaDays = [];

window.initPikaday = () => {
  window.pikaDays.forEach((picker) => {
    picker.destroy();
  });
  $("input.pikaday").each((_index, el) => {
    var picker = new Pikaday({ field: el, format: "YYYY-MM-DD" });
    window.pikaDays.push(picker);
  });
};

document.addEventListener("DOMContentLoaded", initPikaday);
